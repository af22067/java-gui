import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class food {
    private JPanel root;
    private JLabel topLabel;
    private JButton zaruudonButton;
    private JButton zarusobaButton;
    private JButton tendonButton;
    private JButton unajuButton;
    private JButton tempurasettoButton;
    private JButton shiratamaanmitsuButton;
    private JTextArea textArea1;
    private JTextArea textArea2;
    private JButton checkOutButtonButton;
    int sum = 0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("food");
        frame.setContentPane(new food().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order " + food + "?", "Order Confirmation", JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            int largeconfirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like a large portion for free?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION);
            String currentText = textArea1.getText();
            if (largeconfirmation == 0) {
                JOptionPane.showMessageDialog(null, "Thank you for ordering large size " + food + "! It will be served as soon as possible.");
            } else {
                JOptionPane.showMessageDialog(null, "Thank you for ordering normal size " + food + "! It will be served as soon as possible.");
            }
            textArea1.setText(currentText + food + " " + price + "yen" + "\n");

            sum += price;
            textArea2.setText("total " + sum + "yen");

        }
    }

    public food() {
        zaruudonButton.setIcon(new ImageIcon(this.getClass().getResource("./jpg/zaruudon.jpg")));
        zarusobaButton.setIcon(new ImageIcon(this.getClass().getResource("./jpg/zarusoba.jpg")));
        tendonButton.setIcon(new ImageIcon(this.getClass().getResource("./jpg/tendon.jpg")));
        unajuButton.setIcon(new ImageIcon(this.getClass().getResource("./jpg/unaju.jpg")));
        tempurasettoButton.setIcon(new ImageIcon(this.getClass().getResource("./jpg/tensetto.png")));
        shiratamaanmitsuButton.setIcon(new ImageIcon(this.getClass().getResource("./jpg/shiratamaanmitsu.jpg")));


        zaruudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("zaruudon", 500);
            }
        });

        zarusobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("zarusoba", 450);
            }

        });
        tendonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("tendon", 400);
            }
        });
        unajuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("unajubutton", 1000);
            }
        });
        tempurasettoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("tempurasettobutton", 300);
            }
        });
        shiratamaanmitsuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("shiratamaanmitsubutton", 200);
            }
        });
        tempurasettoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("tempurasettobutton", 400);
            }
        });
        checkOutButtonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null ,"Would you like to check out ?" ,
                        "Order Confirmation",JOptionPane.YES_NO_OPTION );
                if(confirmation == 0 ) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " + sum + " yen");
                    textArea1.setText("");
                    textArea2.setText("0");
                    sum = 0;
                }
                }
        });
        }
    private void createUIComponents()
    {
        // TODO: place custom component creation code here
    }



}



